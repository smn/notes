var g_saved;
var g_deleted;

function init()
{
	g_saved = true;
	g_deleted = false;
	if (document.cookie)
	{
		cookies = document.cookie.split(";");
		for (var c in cookies)
		{
			var key = cookies[c].split("=")[0];
			var val = cookies[c].split("=")[1];
			if (key == "theme")
			{
				document.getElementById("theme").value = val;	
				if (typeof pad != "undefined") pad.className = "pad " + val;
				if (typeof thread != "undefined") thread.className = "pad " + val;
			}
		}
	}
	if (thread)
	{
		thread.scrollTop = thread.scrollHeight;
	}
}

function newnote()
{
	var name = prompt("New note name:");
	if (name)
	{
		window.location = "index.php?note=" + name;
	}
}

function onchangetheme(list)
{
	var theme = list.options[list.selectedIndex].value;
	if (typeof pad != "undefined") pad.className = "pad " + theme;
	if (typeof thread != "undefined") thread.className = "pad " + theme;
	document.cookie = "theme=" + theme;
}

function save(archive)
{
	var note = location.search.split("=")[1];
	if (note && !g_deleted)
	{
		var content = pad.innerText;
		if (content)
		{
			var http = new XMLHttpRequest();

			var url = "index.php?note=" + note;
			var params = "pad=" + encodeURIComponent(content);
			if (archive)
			{
				params += "&archive=true";
			}
			http.open("POST", url, true);

			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.setRequestHeader("Content-length", params.length);
			http.setRequestHeader("Connection", "close");

			http.onreadystatechange = function() 
			{
				if(http.readyState == 4 && http.status == 200) 
				{
					if (archive)
					{
						 location.reload();
					}
					else
					{
						g_saved = true;
						document.getElementById('status').textContent = "saved";
					}
				}
			}
			http.send(params);
		}
	}
}

function onNoteChanged()
{
	if (g_saved)
	{
		g_saved = false;
		document.getElementById('status').textContent = "unsaved";

		// launch autosave
		//window.setTimeout(save, 5000);
	}
	//formatjs();
}

function deletenote()
{
	var note = location.search.split("=")[1];
	if (note)
	{
		if (confirm("You are about to delete \"" + note + "\". This cannot be undone. Are your sure?"))
		{
			var http = new XMLHttpRequest();

			var url = "index.php?note=" + note;
			var params = "delete=1";
			http.open("POST", url, true);

			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.setRequestHeader("Content-length", params.length);
			http.setRequestHeader("Connection", "close");

			http.onreadystatechange = function() 
			{
				if(http.readyState == 4 && http.status == 200) 
				{
					g_deleted = true;
					window.location = "index.php";		
				}
			}
			http.send(params);
		}
	}
}

function formatjs()
{
	var htmlContent = pad.innerText;

	htmlContent = htmlContent.replace(/'.*'|\".*\"/g, function (x)
	{
		return "<span class='string'>" + x + "</span>";
	});

	htmlContent = htmlContent.replace(/\b(if|var|for|while)\b/g, function (x)
	{
		return "<span class='keyword'>" + x + "</span>";
	});

	htmlContent = htmlContent.replace(/\d+/g, function (x)
	{
		return "<span class='number'>" + x + "</span>";
	});

	htmlContent = htmlContent.replace(/\/\/.*|\/\*(.|[\r\n])*\*\//g, function (x)
	{
		return "<span class='comment'>" + x + "</span>";
	});

	pad.innerHTML = htmlContent;
}

function executejs()
{
	eval(pad.innerText);
}