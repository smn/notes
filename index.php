<html>

<head>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<script src="client.js"></script>
	<title>Notepad</title>
	 <meta charset="UTF-8">
</head>

<body onload="init()" onbeforeunload="save()">

<?php
	$firstline = "<?php include '../authent.php' ?>";
	$note = $_GET['note'];
	$filename = "./content/" . $note . ".php";
	$version = $_GET['version'];
	if (isset($version))
	{
		$filename = "./content/." . $note . $version . ".php";

	}
	$password = "password";

	function noteNameIsValid()
	{
		global $note;
		return isset($note) && trim($note) != "";
	}

	// Login
	session_start();
	if (isset($_GET["logout"]))
	{
		unset($_SESSION['auth']);
	}

	if (!isset($_SESSION['auth'])) 
	{
		if ($_POST['password'] == $password)
		{
			$_SESSION['auth'] = true;
		}
		else
		{
?>
			Welcome to notes.
			<form method="POST">
				<input type='password' name='password' />
				<input type="submit" name="save" value="Enter"/>
			</form>
		</body>
	</html>

<?php
			exit;
		}
	}

	if (noteNameIsValid() && !file_exists($filename))
	{
		$fp = fopen($filename, 'w');
		fclose($fp);
	}

	// Ajax query: save
	if (isset($_POST['pad']))
	{
		$fp = fopen($filename, 'w');
		fwrite($fp, $firstline);
    	fwrite($fp, $_POST['pad']);
    	fclose($fp);
		if (isset($_POST['archive']))
		{
			$filename = str_replace($note, "." . $note . date('Y-m-d H:i:s'), $filename);
			$fp = fopen($filename, 'w');
			fwrite($fp, $firstline);
    		fwrite($fp, $_POST['pad']);
    		fclose($fp);
		}
  	}
  	else if (isset($_POST["threadinput"]))
	{
		file_put_contents("./content/thread.php", date('Y-m-d H:i:s') . ":" . $_POST["threadinput"] . "\r\n", FILE_APPEND);
	}

	// Ajax query: delete
  	if (isset($_POST['delete']))
  	{
  		$result = unlink($filename);
	}

  	function readcontent()
  	{
		global $filename, $firstline;
  		$file = fopen($filename,"r");
		$content = fread($file,filesize($filename));
		fclose($file);
		$content = str_replace($firstline, "", $content);
		$content = htmlspecialchars($content);
		return $content;
  	}

  	//menu
  	$snapshots = "";
	$files = scandir("./content/");
	foreach ($files as $file)
	{
		$file = str_replace(".php", "", $file);
    	if (strpos($file, ".") !== 0)
    	{
    		if ($file != $note)
    		{
    			echo "<a class='item' href='index.php?note=" . urlencode($file) . "'>" . $file . "</a>";
    		}
    		else
    		{
    			echo "<span class='item'>" . $file . "</span>";
    		}
    	}
    	else if (strpos($file, $note) !== FALSE)
    	{
    		$v = str_replace("." . $note, "", $file);
    		if ($v === $version)
    		{
    			$snapshots = $snapshots . "<span class='item'>" . $v . "</span>";
    		}
    		else
    		{
    			$snapshots = $snapshots . "<a class='item' href='index.php?note=" . urlencode($note) . "&version=" . $v . "'>" . $v . "</a>";
    		}
    	}
	}
	if (isset($version))
	{
		$snapshots = $snapshots . "<a class='item' href='index.php?note=" . urlencode($note) . "'>now</a>";
	}
	else
	{
		$snapshots = $snapshots . "<span class='item'>now</span>";
	}
?>
<button onclick="newnote()">Create a new note</button>
theme:
<select id="theme" onChange="onchangetheme(this)">
  <option>classic</option>
  <option>monokai</option>
  <option>terminal</option>
</select> 
<a class="item" href="index.php?logout=1">log out</a>

<!-- current note -->
<?php 
if ($note == "thread")
{
?>
<form method="POST">
	<pre class="pad classic" id="thread">
		<?php
		$handle = @fopen("./content/thread.php", "r");
		if ($handle) 
		{
			//skip first line
			fgets($handle);
		    while (($buffer = fgets($handle)) !== false) 
		    {
		    	// string substr ( string $string , int $start [, int $length ] )
		        echo "<div>";
		        echo "<span>" . substr($buffer, 0, 19) . "</span>";
		        echo "&nbsp;";
		        echo "<span class='threadtext'>" . htmlspecialchars(substr($buffer, 20)) . "</span>";
		       	echo "<div>";
		    }
		    fclose($handle);
		}
		?>
	</pre>
	<input type="text" class="threadinput" name="threadinput" autofocus>
</form>
<?php
}
else if (noteNameIsValid())
{
?>
	<pre contenteditable="<?php echo isset($version) ?>" name="pad" id="pad" class="pad classic" oninput="onNoteChanged()"><?php echo readcontent()?></pre>
	<button onclick="save()" <?php if (isset($version)) echo "disabled"; ?>>Save note</button>
	<span class="item" id="status">saved</span>
	<a class='item' target="blank" href="<?php echo $filename ?>">view raw</a>
	<button onclick="deletenote()" <?php if (isset($version)) echo "disabled"; ?>>Delete note</button>
	labs:
	<button onclick="formatjs()">Format JS</button>
	<button onclick="executejs()">Execute JS</button>
	<button onclick="save(true)" <?php if (isset($version)) echo "disabled"; ?>>Take a snapshot</button>
	<?php echo $snapshots; ?>
<?php
}
else
{?>
	<br><span class='item'>Please select a note or create a new one.</span>
<?php
}
?>
</body>

</html>
